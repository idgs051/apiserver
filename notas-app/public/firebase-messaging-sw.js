importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js');


const firebaseConfig = {
  apiKey: "AIzaSyBKmAw4r75d323zKAln1QUOnMSP7UeWz2Y",
  authDomain: "test-push-7d903.firebaseapp.com",
  projectId: "test-push-7d903",
  storageBucket: "test-push-7d903.appspot.com",
  messagingSenderId: "1098456240930",
  appId: "1:1098456240930:web:ceacb3fc5fd023bae08b7b",
  measurementId: "G-ZZ6EHTHZGH"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
    console.log('Recibiendo mensaje en segundo plano')
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: '../img/40.png',
    }

    self.registration.showNotification(tituloNotificacion, options)
})