import React from 'react';
import Navegation from './Routes/Navegation';
import './styles/Global.css'; 

const App = () => {
  return (
    <div>
      <Navegation />
    </div>
  );
};

export default App;
