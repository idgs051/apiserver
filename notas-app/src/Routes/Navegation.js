import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from '../pages/Home';
import Notas from '../pages/Notas';


const Navegation = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Notas' element={<Notas />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Navegation;