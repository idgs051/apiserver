import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getMessaging, getToken} from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyBKmAw4r75d323zKAln1QUOnMSP7UeWz2Y",
  authDomain: "test-push-7d903.firebaseapp.com",
  projectId: "test-push-7d903",
  storageBucket: "test-push-7d903.appspot.com",
  messagingSenderId: "1098456240930",
  appId: "1:1098456240930:web:ceacb3fc5fd023bae08b7b",
  measurementId: "G-ZZ6EHTHZGH"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const messaging = getMessaging(app);