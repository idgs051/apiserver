import React from 'react';
import { getAuth, signInAnonymously } from 'firebase/auth';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import '../styles/Home.css'; 
import portada from '../assets/notepad.png'; 

const Home = () => {
  const login = () => {
    signInAnonymously(getAuth()).then((usuario) => console.log(usuario));

  };

  return (
    <div className='container'>
      <img className='img' src={portada} alt='Portada' />
      <Button type='primary' onClick={login}>

        <Link to='/Notas'>Iniciar sesión</Link>

      </Button>
     
    </div>
  );
};

export default Home;
