import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getToken, onMessage } from 'firebase/messaging';
import { messaging } from '../firebase';

const Notas = () => {
  const getTokenNotification = async () => {
    const token = await getToken(messaging, {
      vapidKey: 'BBF9SoksP_FnaVa_sYSiqPjZOtDx52p-XoKK0UzmiCif7epGBIPZBLtbpec8ZTQsYxAWUkLAEJeIASEqGaa5Mts'
    }).catch((err) => console.log('No se pudo obtener el token', err))

    if (token) {
      console.log('Token: ', token)
    } if (!token) {
      console.log('No hay token disponible')
    }
  }

  const notificarme = () => {
    if (!window.Notification) {
      console.log('Este navegador no soporta notificaciones');
      return;
    }
    if(Notification.permission === 'granted'){
      getTokenNotification();
    }else if (Notification.permission !== 'denied' || Notification.permission === 'default') {
      Notification.requestPermission((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          getTokenNotification();
        }
      });
    }; 
  };
  notificarme();


  useEffect(() => {
    getTokenNotification()
    onMessage(messaging, message => {
      console.log('onMessage: ', message)
      toast(message.notification.title)
    })
  }, []);

  const [notes, setNotes] = useState([]);
  const [newNote, setNewNote] = useState({ title: '', description: '' });

  useEffect(() => {
    axios
      .get('http://localhost:3000/api/note')
      .then((response) => setNotes(response.data))
      .catch((error) => console.error('Error fetching notes:', error));
  }, []);

  // useEffect(() => {
  //   const unsubscribe = onMessage(messaging, (payload) => {
  //     toast.success(`Nueva notificación: ${payload.notification.body}`);
  //   });

  //   getToken(messaging)
  //     .then((currentToken) => {
  //       if (currentToken) {
  //         console.log('Token FCM:', currentToken);
  //       } else {
  //         console.log('No se pudo obtener el token FCM. Asegúrate de estar autorizado.');
  //       }
  //     })
  //     .catch((err) => {
  //       console.error('Error al obtener el token FCM:', err);
  //     });

  //   return () => {
  //     unsubscribe();
  //   };
  // }, []);

  const handleNoteSubmit = () => {
    if (newNote.title.trim() === '') {
      alert('El título no puede estar vacío.');
      return;
    }
    if (newNote.description.trim() === '') {
      alert('La nota no puede estar vacía.');
      return;
    }

    axios
      .post('http://localhost:3000/api/note', newNote)
      .then((response) => {
        setNotes([...notes, response.data]);
        setNewNote({ title: '', description: '' });
      })
      .catch((error) => console.error('Error creating note:', error));
  };


  return (
    <div style={containerStyle} className="App">
      <ToastContainer />
      <header className="App-header">
        <form>
          <input
            type="description"
            placeholder="Título"
            style={inputStyle}
            value={newNote.title}
            onChange={(e) => setNewNote({ ...newNote, title: e.target.value })}
          />
          <input
            type="description"
            placeholder="Descripción de la nota"
            style={inputStyle}
            value={newNote.description}
            onChange={(e) => setNewNote({ ...newNote, description: e.target.value })}
          />
          <button type="button" style={buttonStyle} onClick={handleNoteSubmit}>
            Crear nota
          </button>
        </form>
        {notes.length === 0 ? (
          <p>No hay notas disponibles.</p>
        ) : (
          <ul style={ulStyle}>
            {notes.map((note) => (
              <li key={note._id} style={liStyle}>
                <h3>{note.title}</h3>
                <p>{note.description}</p>
              </li>
            ))}
          </ul>
        )}
      </header>
    </div>
  );
};

export default Notas;

const containerStyle = {
  maxWidth: '600px',
  margin: '0 auto',
  padding: '20px',
  backgroundColor: '#fff',
  borderRadius: '8px',
  boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
};

const inputStyle = {
  width: '100%',
  padding: '10px',
  marginBottom: '10px',
  border: '1px solid #ccc',
  borderRadius: '4px',
};

const buttonStyle = {
  padding: '10px 20px',
  backgroundColor: '#007bff',
  color: '#fff',
  border: 'none',
  borderRadius: '4px',
  cursor: 'pointer',
};

const ulStyle = {
  listStyle: 'none',
  padding: '0',
};

const liStyle = {
  border: '1px solid #ccc',
  borderRadius: '4px',
  padding: '10px',
  marginBottom: '10px',
  backgroundColor: '#fff',
  boxShadow: '0 0 5px rgba(0, 0, 0, 0.1)',
};
